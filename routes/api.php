<?php

use Illuminate\Http\Request;

Route::post('/test', function (Request $request) {
    return [
        'status' => 'success',
        'email' => $request->email,
    ];
});
